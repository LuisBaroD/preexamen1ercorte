package com.example.preexamen;

import java.io.Serializable;

public class IMCS implements Serializable {

    private float IMC;
    private float Peso;
    private float Altura;

    public IMCS(float IMC, float peso, float altura) {
        this.IMC = IMC;
        Peso = peso;
        Altura = altura;
    }

    public IMCS() {

    }

    public float getIMC() {
        return IMC;
    }

    public float getPeso() {
        return Peso;
    }

    public float getAltura() {
        return Altura;
    }

    public void setIMC(float IMC) {
        this.IMC = IMC;
    }

    public void setPeso(float peso) {
        Peso = peso;
    }

    public void setAltura(float altura) {
        Altura = altura;
    }

    public float calculoIMC(){
        float Imc = this.Peso / (this.Altura * this.Altura);
        return Imc;
    }
}
