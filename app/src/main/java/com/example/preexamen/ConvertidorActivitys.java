package com.example.preexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class ConvertidorActivitys extends AppCompatActivity {

    private TextView lblEncabezado;
    private TextView lblNombre;
    private EditText txtCantidad;
    private RadioButton rbtnCel;
    private RadioButton rbtnFar;
    private TextView lblResultado;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnSalir;
    private Convertidor convertir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convertidor_activitys);

        lblNombre = (TextView) findViewById(R.id.lblNombre);
        txtCantidad = (EditText) findViewById(R.id.txtGrados);
        rbtnCel = (RadioButton) findViewById(R.id.rbtnP1);
        rbtnFar = (RadioButton) findViewById(R.id.rbtnP2);
        lblResultado = (TextView) findViewById(R.id.lblResultado);
        btnCalcular = (Button) findViewById(R.id.btnConvierte);
        btnLimpiar = (Button) findViewById(R.id.btnBorrar);
        btnSalir = (Button) findViewById(R.id.btnVolver);
        convertir = new Convertidor();

        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("name");
        lblNombre.setText(nombre);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String grados = txtCantidad.getText().toString();

                if(grados.matches("")){
                    Toast.makeText(ConvertidorActivitys.this,"Debes ingresar una cantidad",Toast.LENGTH_SHORT).show();
                }
                else if(rbtnFar.isChecked() == false && rbtnCel.isChecked() == false){
                    Toast.makeText(ConvertidorActivitys.this,"Debes seleccionar una conversion",Toast.LENGTH_SHORT).show();
                }
                else{
                    convertir.setGrados(Integer.parseInt(grados));
                    if(rbtnCel.isChecked() == true){
                        String res = String.valueOf(convertir.covertirAFahrenheit());
                        lblResultado.setText("Resultado: " + res + " grados F");
                    }else if(rbtnFar.isChecked() == true){
                        String res = String.valueOf(convertir.covertirACelsius());
                        lblResultado.setText("Resultado: " + res + " grados C");
                    }
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtCantidad.setText("");
                lblResultado.setText("");
                rbtnCel.setChecked(false);
                rbtnFar.setChecked(false);
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}

