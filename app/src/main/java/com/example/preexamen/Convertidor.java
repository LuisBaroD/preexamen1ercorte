package com.example.preexamen;

import java.io.Serializable;

public class Convertidor implements Serializable {

    private int Grados;

    public Convertidor(int grados) {
        Grados = grados;
    }

    public Convertidor() {

    }

    public int getGrados() {
        return Grados;
    }

    public void setGrados(int grados) {
        Grados = grados;
    }

    public float covertirACelsius(){
        float res = (this.Grados - 32) * 5/9;
        return res;
    }

    public  float covertirAFahrenheit(){
        float res = (this.Grados * 9 / 5) + 32;
        return res;
    }
}
