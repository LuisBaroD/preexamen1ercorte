package com.example.preexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class IMC extends AppCompatActivity {

    private IMCS imcs;
    private TextView lbsNombre;
    private Button lblRegresar;
    private Button lblCalcular;
    private Button lbllimpiar;
    private EditText msgMetros;
    private EditText msgCentimetros;
    private EditText msgPeso;
    private TextView ResulIMC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc);

        lbsNombre = (TextView) findViewById(R.id.lbsNombre);
        lblRegresar = (Button) findViewById(R.id.BtnRegresar);
        lblCalcular = (Button) findViewById(R.id.BtnCalcular);
        lbllimpiar = (Button) findViewById(R.id.BtnLimpiar);
        msgMetros = (EditText) findViewById(R.id.msgMetros);
        msgCentimetros = (EditText) findViewById(R.id.msgCentimetros);
        msgPeso = (EditText) findViewById(R.id.msgPeso);
        ResulIMC = (TextView) findViewById(R.id.ResIMC);
        imcs = new IMCS();

        Bundle datos = getIntent().getExtras();
        String cliente = datos.getString("name");
        lbsNombre.setText(cliente);


        lblCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Metro = msgMetros.getText().toString();
                String Centimentro = msgCentimetros.getText().toString();
                String Peso = msgPeso.getText().toString();

                if(msgMetros.getText().toString().matches("")){
                    Toast.makeText(IMC.this,"Debes llenar todos los campos para calcular",Toast.LENGTH_SHORT).show();
                }
                else if(msgCentimetros.getText().toString().matches("")){
                    Toast.makeText(IMC.this,"Debes llenar todos los campos para calcular",Toast.LENGTH_SHORT).show();
                } else if(msgPeso.getText().toString().matches("")){
                    Toast.makeText(IMC.this,"Debes llenar todos los campos para calcular",Toast.LENGTH_SHORT).show();
                }
                else {

                    imcs.setAltura(Float.parseFloat(Metro + "." + Centimentro));
                    imcs.setPeso(Float.parseFloat(Peso));

                    ResulIMC.setText(imcs.calculoIMC() + "");

                }

            }
        });


        lbllimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                msgMetros.setText("");
                msgCentimetros.setText("");
                msgPeso.setText("");

                ResulIMC.setText("");
            }
        });


        lblRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
