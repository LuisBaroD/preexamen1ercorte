package com.example.preexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity {

    private EditText txtNombre;
    private Button btnConver;
    private Button btnCerrar;
    private Button btnIMC;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtNombre = (EditText) findViewById(R.id.txtnombre);
        btnConver = (Button) findViewById(R.id.btnConvetidor);
        btnIMC = (Button) findViewById(R.id.btnIMC);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);

        btnIMC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();

                if (nombre.matches("")){
                    Toast.makeText(MainActivity.this, "Falto capturar Nombre" ,
                            Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Intent intent = new Intent(MainActivity.this,IMC.class);
                    intent.putExtra("name",nombre);
                    startActivity(intent);
                }

            }

        });

        btnConver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();

                if(nombre.matches("")){
                    Toast.makeText(MainActivity.this,"Debes ingresar tu nombre primero",Toast.LENGTH_SHORT).show();
                }
                else{
                    Intent intent = new Intent(MainActivity.this,ConvertidorActivitys.class);
                    intent.putExtra("name",nombre);
                    startActivity(intent);
                }
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}

